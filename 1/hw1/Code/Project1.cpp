#include "mainwindow.h"
#include "math.h"
#include "ui_mainwindow.h"
#include <QtGui>

/***********************************************************************
  This is the only file you need to change for your assignment. The
  other files control the UI (in case you want to make changes.)
************************************************************************/

/***********************************************************************
  The first eight functions provide example code to help get you started
************************************************************************/


// Convert an image to grayscale
void MainWindow::BlackWhiteImage(QImage *image)
{
    for(int r=0;r<image->height();r++)
        for(int c=0;c<image->width();c++)
        {
            QRgb pixel = image->pixel(c, r);
            double red = (double) qRed(pixel);
            double green = (double) qGreen(pixel);
            double blue = (double) qBlue(pixel);

            // Compute intensity from colors - these are common weights
            double intensity = 0.3*red + 0.6*green + 0.1*blue;

            image->setPixel(c, r, qRgb( (int) intensity, (int) intensity, (int) intensity));
        }
}

// Add random noise to the image
void MainWindow::AddNoise(QImage *image, double mag, bool colorNoise)
{
    int noiseMag = mag*2;

    for(int r=0;r<image->height();r++)
        for(int c=0;c<image->width();c++)
        {
            QRgb pixel = image->pixel(c, r);
            int red = qRed(pixel), green = qGreen(pixel), blue = qBlue(pixel);

            // If colorNoise, add color independently to each channel
            if(colorNoise)
            {
                red += rand()%noiseMag - noiseMag/2;
                green += rand()%noiseMag - noiseMag/2;
                blue += rand()%noiseMag - noiseMag/2;
            }
            // otherwise add the same amount of noise to each channel
            else
            {
                int noise = rand()%noiseMag - noiseMag/2;
                red += noise; green += noise; blue += noise;
            }
            image->setPixel(c, r, qRgb(max(0, min(255, red)), max(0, min(255, green)), max(0, min(255, blue))));
        }
}

// Downsample the image by 1/2
void MainWindow::HalfImage(QImage &image)
{
    int w = image.width();
    int h = image.height();
    QImage buffer = image.copy();

    // Reduce the image size.
    image = QImage(w/2, h/2, QImage::Format_RGB32);

    // Copy every other pixel
    for(int r=0;r<h/2;r++)
        for(int c=0;c<w/2;c++)
             image.setPixel(c, r, buffer.pixel(c*2, r*2));
}

// Round float values to the nearest integer values and make sure the value lies in the range [0,255]
QRgb restrictColor(double red, double green, double blue)
{
    int r = (int)(floor(red+0.5));
    int g = (int)(floor(green+0.5));
    int b = (int)(floor(blue+0.5));

    return qRgb(max(0, min(255, r)), max(0, min(255, g)), max(0, min(255, b)));
}

// Normalize the values of the kernel to sum-to-one
void NormalizeKernel(double *kernel, int kernelWidth, int kernelHeight)
{
    double denom = 0.000001; int i;
    for(i=0; i<kernelWidth*kernelHeight; i++)
        denom += kernel[i];
    for(i=0; i<kernelWidth*kernelHeight; i++)
        kernel[i] /= denom;
}

// Here is an example of blurring an image using a mean or box filter with the specified radius.
// This could be implemented using separable filters to make it much more efficient, but it's not done here.
// Note: This function is written using QImage form of the input image. But all other functions later use the double form
void MainWindow::MeanBlurImage(QImage *image, int radius)
{
    if(radius == 0)
        return;
    int size = 2*radius + 1; // This is the size of the kernel

    // Note: You can access the width and height using 'imageWidth' and 'imageHeight' respectively in the functions you write
    int w = image->width();
    int h = image->height();

    // Create a buffer image so we're not reading and writing to the same image during filtering.
    // This creates an image of size (w + 2*radius, h + 2*radius) with black borders (zero-padding)
    QImage buffer = image->copy(-radius, -radius, w + 2*radius, h + 2*radius);

    // Compute the kernel to convolve with the image
    double *kernel = new double [size*size];

    for(int i=0;i<size*size;i++)
        kernel[i] = 1.0;

    // Make sure kernel sums to 1
    NormalizeKernel(kernel, size, size);

    // For each pixel in the image...
    for(int r=0;r<h;r++)
    {
        for(int c=0;c<w;c++)
        {
            double rgb[3];
            rgb[0] = rgb[1] = rgb[2] = 0.0;

            // Convolve the kernel at each pixel
            for(int rd=-radius;rd<=radius;rd++)
                for(int cd=-radius;cd<=radius;cd++)
                {
                     // Get the pixel value
                     //For the functions you write, check the ConvertQImage2Double function to see how to get the pixel value
                     QRgb pixel = buffer.pixel(c + cd + radius, r + rd + radius);

                     // Get the value of the kernel
                     double weight = kernel[(rd + radius)*size + cd + radius];

                     rgb[0] += weight*(double) qRed(pixel);
                     rgb[1] += weight*(double) qGreen(pixel);
                     rgb[2] += weight*(double) qBlue(pixel);
                }
            // Store the pixel in the image to be returned
            // You need to store the RGB values in the double form of the image
            image->setPixel(c, r, restrictColor(rgb[0],rgb[1],rgb[2]));
        }
    }
    // Clean up (use this carefully)
    delete[] kernel;
}

// Convert QImage to a matrix of size (imageWidth*imageHeight)*3 having double values
void MainWindow::ConvertQImage2Double(QImage image)
{
    // Global variables to access image width and height
    imageWidth = image.width();
    imageHeight = image.height();

    // Initialize the global matrix holding the image
    // This is how you will be creating a copy of the original image inside a function
    // Note: 'Image' is of type 'double**' and is declared in the header file (hence global variable)
    // So, when you create a copy (say buffer), write "double** buffer = new double ....."
    Image = new double* [imageWidth*imageHeight];
    for (int i = 0; i < imageWidth*imageHeight; i++)
            Image[i] = new double[3];

    // For each pixel
    for (int r = 0; r < imageHeight; r++)
        for (int c = 0; c < imageWidth; c++)
        {
            // Get a pixel from the QImage form of the image
            QRgb pixel = image.pixel(c,r);

            // Assign the red, green and blue channel values to the 0, 1 and 2 channels of the double form of the image respectively
            Image[r*imageWidth+c][0] = (double) qRed(pixel);
            Image[r*imageWidth+c][1] = (double) qGreen(pixel);
            Image[r*imageWidth+c][2] = (double) qBlue(pixel);
        }
}

// Convert the matrix form of the image back to QImage for display
void MainWindow::ConvertDouble2QImage(QImage *image)
{
    for (int r = 0; r < imageHeight; r++)
        for (int c = 0; c < imageWidth; c++)
            image->setPixel(c, r, restrictColor(Image[r*imageWidth+c][0], Image[r*imageWidth+c][1], Image[r*imageWidth+c][2]));
}


/**************************************************
 TIME TO WRITE CODE
**************************************************/

/**************************************************
 TASK 1
**************************************************/

// Helper function to delete an allocated image
void DeleteCopy(double** imageCopy, int size)
/*
 * imageCopy: image copy to be deleted
 * size: size of image to be deleted
*/
{
    // Delete the copy subarrays
    for (int i = 0; i < size; i++)
        delete[] imageCopy[i];
    // Delete the copy array
    delete[] imageCopy;
}

// Convolve the image with the kernel
void MainWindow::Convolution(double** image, double *kernel, int kernelWidth, int kernelHeight, bool add)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * kernel: 1-D array of kernel values
 * kernelWidth: width of the kernel
 * kernelHeight: height of the kernel
 * add: a boolean variable (taking values true or false)
*/
{
    // Width and height must be odd
    if (kernelWidth % 2 == 0 || kernelHeight % 2 == 0)
    {
        return;
    }

    int w = imageWidth;
    int h = imageHeight;

    // Create a buffer image so we're not reading and writing to the same image during filtering.
    // This creates an image of size (w + kernelWidth - 1, h + kernelHeight - 1) with black borders (zero-padding)
    int pad = (w + kernelWidth - 1) * (h + kernelHeight - 1);
    double** buffer = new double* [pad];
    for (int i = 0; i < pad; i++)
    {
        buffer[i] = new double[3];
        // initialize to zero
        buffer[i][0] = 0, buffer[i][1] = 0, buffer[i][2] = 0;
    }

    // For each pixel in the original
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            int bufferPos = 0;                                      // This assignment is for clarity; it will optimize away
            bufferPos += kernelHeight / 2 * (w + kernelWidth - 1);  // Skip the top edge
            bufferPos += (w + kernelWidth - 1) * r;                 // Forward to the current row
            bufferPos += kernelWidth / 2;                           // Skip the left edge in the current row
            bufferPos += c;                                         // Forward to the current column

            // Assign the red, green and blue channel values to the 0, 1 and 2 channels
            // of the double form of the image respectively
            buffer[bufferPos][0] = image[r*w+c][0];
            buffer[bufferPos][1] = image[r*w+c][1];
            buffer[bufferPos][2] = image[r*w+c][2];
        }

    // For each pixel in the image...
    for (int r = 0; r < h; r++)
    {
        for (int c = 0; c < w; c++)
        {
            double rgb[3];
            rgb[0] = rgb[1] = rgb[2] = 0.0;

            // Convolve the kernel at each pixel
            for (int rd = 0; rd < kernelHeight; rd++)
                for (int cd = 0; cd < kernelWidth; cd++)
                {
                     // Get the pixel value
                     double* pixel = buffer[(r + rd) * (w + kernelWidth - 1) + c + cd];

                     // Get the value of the kernel
                     double weight = kernel[rd * kernelWidth + cd];

                     rgb[0] += weight*pixel[0];
                     rgb[1] += weight*pixel[1];
                     rgb[2] += weight*pixel[2];
                }
            // Store the pixel in the image to be returned
            if (add)
            {
                rgb[0] += 128, rgb[1] += 128, rgb[2] += 128;
            }
            image[r*w+c][0] = rgb[0];
            image[r*w+c][1] = rgb[1];
            image[r*w+c][2] = rgb[2];
        }
    }
    // Clean up
    // Delete the buffer subarrays
    DeleteCopy(buffer, pad);
}

/**************************************************
 TASK 2
**************************************************/

// Apply the 2-D Gaussian kernel on an image to blur it
void MainWindow::GaussianBlurImage(double** image, double sigma)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * sigma: standard deviation for the Gaussian kernel
*/
{
    // The radius of the Gaussian blur
    int radius = (int)(ceil(3*sigma));
    // The kernel is square
    int kernelWidth = 2*radius+1, kernelHeight = 2*radius+1;

    // Initialize the kernel
    double* kernel = new double[kernelWidth * kernelHeight];

    // Calculate the scalar for this sigma (used in the Gaussian function)
    double scalar = 1.0 / (2 * M_PI * sigma * sigma);

    // For each row
    for (int h = -radius; h <= radius; h++)
    {
        // For each column
        for (int w = -radius; w <= radius; w++)
        {
            // Calculate and assign the kernel value for the Gaussian blur at that location
            kernel[(h + radius) * kernelWidth + (w + radius)]
                = scalar * exp(- (h * h + w * w) / (2 * sigma * sigma));
        }
    }

    // Normalize the kernel to sum to 1
    NormalizeKernel(kernel, kernelWidth, kernelHeight);

    // Convolve the kernel with the image, don't set add
    Convolution(image, kernel, kernelWidth, kernelHeight, false);

    delete[] kernel;
}

/**************************************************
 TASK 3
**************************************************/

// Perform the Gaussian Blur first in the horizontal direction and then in the vertical direction
void MainWindow::SeparableGaussianBlurImage(double** image, double sigma)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * sigma: standard deviation for the Gaussian kernel
*/
{
    // The radius of the Gaussian blur
    int radius = (int)(ceil(3*sigma));

    // Initialize the kernel (will be same size for either direction)
    double* kernel = new double[2*radius+1];

    // Calculate the scalar for this sigma (used in the Gaussian function)
    double scalar = 1.0 / sqrt(2 * M_PI * sigma * sigma);

    // The values of the kernel are the same in either direction (since the kernel is a 1D array
    // and the function used does not change with the direction).
    // Therefore, the only thing that will be changed is kernelWidth and kernelHeight before
    // convolution.
    for (int i = -radius; i <= radius; i++)
    {
        // Calculate and assign the kernel value for the Gaussian blur at that location
        kernel[i + radius] = scalar * exp(- (i * i) / (2 * sigma * sigma));
    }

    // Normalize the kernel to sum to 1
    NormalizeKernel(kernel, 2*radius+1, 1);

    // HORIZONTAL DIRECTION

    // The kernel is 1D
    int kernelWidth = 2*radius+1, kernelHeight = 1;

    // Convolve the kernel with the image, don't set add
    Convolution(image, kernel, kernelWidth, kernelHeight, false);

    // VERTICAL DIRECTION

    // The kernel is 1D
    kernelWidth = 1, kernelHeight = 2*radius+1;

    Convolution(image, kernel, kernelWidth, kernelHeight, false);

    delete[] kernel;
}

/********** TASK 4 (a) **********/

// Compute the First derivative of an image along the horizontal direction and then apply Gaussian blur.
void MainWindow::FirstDerivImage_x(double** image, double sigma)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * sigma: standard deviation for the Gaussian kernel
*/
{
    // Set the kernel dimentions
    int kernelWidth  = 3;
    int kernelHeight = 1;

    // Initialize the kernel
    double* kernel = new double[3];
    kernel[0] = -1, kernel[1] = 0, kernel[2] = 1;

    // Convolve the kernel with the image, set add
    Convolution(image, kernel, kernelWidth, kernelHeight, true);

    // Apply gaussian blur
    GaussianBlurImage(image, sigma);

    delete[] kernel;
}

/********** TASK 4 (b) **********/

// Compute the First derivative of an image along the vertical direction and then apply Gaussian blur.
void MainWindow::FirstDerivImage_y(double** image, double sigma)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * sigma: standard deviation for the Gaussian kernel
*/
{
    // Set the kernel dimentions
    int kernelWidth  = 1;
    int kernelHeight = 3;

    // Initialize the kernel
    double* kernel = new double[3];
    kernel[0] = -1, kernel[1] = 0, kernel[2] = 1;

    // Convolve the kernel with the image, set add
    Convolution(image, kernel, kernelWidth, kernelHeight, true);

    // Apply gaussian blur
    GaussianBlurImage(image, sigma);

    delete[] kernel;
}

/********** TASK 4 (c) **********/

// Compute the Second derivative of an image using the Laplacian operator and then apply Gaussian blur
void MainWindow::SecondDerivImage(double** image, double sigma)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * sigma: standard deviation for the Gaussian kernel
*/
{
    // Set the kernel dimentions
    int kernelWidth  = 3;
    int kernelHeight = 3;

    // Initialize the kernel
    double* kernel = new double[9];
    kernel[0] =  0, kernel[1] =  1, kernel[2] =  0;
    kernel[3] =  1, kernel[4] = -4, kernel[5] =  1;
    kernel[6] =  0, kernel[7] =  1, kernel[8] =  0;

    // Convolve the kernel with the image, set add
    Convolution(image, kernel, kernelWidth, kernelHeight, true);

    // Apply gaussian blur
    GaussianBlurImage(image, sigma);

    delete[] kernel;
}

/**************************************************
 TASK 5
**************************************************/

// Helper function for copying an image, which is required multiple times during the following
// functions.
double** CopyImage(double** image, int w, int h)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * w:     width of the input image
 * h:     height of the input image
*/
{
    double** imageCopy = new double* [w*h];
    for (int i = 0; i < w*h; i++)
        imageCopy[i] = new double[3];

    // For each pixel
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            // Assign the red, green and blue channel values to the 0, 1 and 2 channels
            imageCopy[r*w+c][0] = image[r*w+c][0];
            imageCopy[r*w+c][1] = image[r*w+c][1];
            imageCopy[r*w+c][2] = image[r*w+c][2];
        }

    return imageCopy;
}

// Sharpen an image by subtracting the image's second derivative from the original image
void MainWindow::SharpenImage(double** image, double sigma, double alpha)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * sigma: standard deviation for the Gaussian kernel
 * alpha: constant by which the second derivative image is to be multiplied to before subtracting it from the original image
*/
{
    // First, copy the image to apply the second derivative to
    int w = imageWidth;
    int h = imageHeight;

    double** copySecondDeriv = CopyImage(image, w, h);

    // Pass the copy into the second derivative function
    SecondDerivImage(copySecondDeriv, sigma);

    // Now, subtract the second derivative from the original image to sharpen
    // For each pixel
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            // Mulitply the subtraction by the value Alpha
            // Add 128 to each value to counteract the addition done to the second derivative
            image[r*w+c][0] = image[r*w+c][0] - alpha * (copySecondDeriv[r*w+c][0] - 128);
            image[r*w+c][1] = image[r*w+c][1] - alpha * (copySecondDeriv[r*w+c][1] - 128);
            image[r*w+c][2] = image[r*w+c][2] - alpha * (copySecondDeriv[r*w+c][2] - 128);
        }

    // Clean up
    DeleteCopy(copySecondDeriv, w*h);
}

/**************************************************
 TASK 6
**************************************************/

// Display the magnitude and orientation of the edges in an image using the Sobel operator in both X and Y directions
void MainWindow::SobelImage(double** image)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * NOTE: image is grayscale here, i.e., all 3 channels have the same value which is the grayscale value
*/
{
    // First, copy the image twice to apply the operator in both directions to
    int w = imageWidth;
    int h = imageHeight;

    double** sobelX = new double* [w*h];
    for (int i = 0; i < w*h; i++)
        sobelX[i] = new double[3];

    double** sobelY = new double* [w*h];
    for (int i = 0; i < w*h; i++)
        sobelY[i] = new double[3];

    // For each pixel
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            // Assign the red, green and blue channel values to the 0, 1 and 2 channels
            // The images are in grayscale, but our convolution function only works in
            // three dimentions, so it's best to do this anyway
            sobelX[r*w+c][0] = sobelY[r*w+c][0] = image[r*w+c][0];
            sobelX[r*w+c][1] = sobelY[r*w+c][1] = image[r*w+c][1];
            sobelX[r*w+c][2] = sobelY[r*w+c][2] = image[r*w+c][2];
        }

    // CONVOLUTION
    // Define the kernel dimentions
    int kernelWidth  = 3;
    int kernelHeight = 3;

    // Initialize the kernel (the same kernel can be used for both convolutions)
    double* kernel = new double[kernelWidth * kernelHeight];

    // Define the horizontal sobel operator
    kernel[0] = -1, kernel[1] =  0, kernel[2] =  1;
    kernel[3] = -2, kernel[4] =  0, kernel[5] =  2;
    kernel[6] = -1, kernel[7] =  0, kernel[8] =  1;

    // Convolve the horizontal kernel
    Convolution(sobelX, kernel, kernelWidth, kernelHeight, false);

    // Define the vertical sobel operator
    kernel[0] =  1, kernel[1] =  2, kernel[2] =  1;
    kernel[3] =  0, kernel[4] =  0, kernel[5] =  0;
    kernel[6] = -1, kernel[7] = -2, kernel[8] = -1;

    // Convolve the vertical kernel
    Convolution(sobelY, kernel, kernelWidth, kernelHeight, false);

    // IMAGE ASSIGNMENT
    // For each pixel
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            // Grayscale so only one needs to be tested
            // Divide by 8 to avoid spurious edges
            double sobelXPix = sobelX[r*w+c][0] / 8;
            double sobelYPix = sobelY[r*w+c][0] / 8;
            // Find the magnitude (grayscale so only one value needs be tested)
            int mag   = sqrt(sobelXPix*sobelXPix + sobelYPix*sobelYPix);
            // Find the orientation (grayscale so only one value needs be tested)
            int orien = atan2(sobelYPix, sobelXPix);

            // Use the following 3 lines of code to set the image pixel values after computing magnitude and orientation
            // Here 'mag' is the magnitude and 'orien' is the orientation angle in radians to be computed using atan2 function
            // (sin(orien) + 1)/2 converts the sine value to the range [0,1]. Similarly for cosine.
            image[r*imageWidth+c][0] = mag*4.0*((sin(orien) + 1.0)/2.0);
            image[r*imageWidth+c][1] = mag*4.0*((cos(orien) + 1.0)/2.0);
            image[r*imageWidth+c][2] = mag*4.0 - image[r*imageWidth+c][0] - image[r*imageWidth+c][1];
        }

    // Delete the copy subarrays
    for (int i = 0; i < w*h; i++)
    {
        delete[] sobelX[i];
        delete[] sobelY[i];
    }
    // Delete the copy array
    delete[] sobelX;
    delete[] sobelY;
}

/**************************************************
 TASK 7
**************************************************/

// Compute the RGB values at a given point in an image using bilinear interpolation.
void MainWindow::BilinearInterpolation(double** image, double x, double y, double rgb[3])
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * x: x-coordinate (corresponding to columns) of the position whose RGB values are to be found
 * y: y-coordinate (corresponding to rows) of the position whose RGB values are to be found
 * rgb[3]: array where the computed RGB values are to be stored
*/
{
    int w = imageWidth;
    int h = imageHeight;

    // If outside image
    if (x < 0 || x > w - 1 || y < 0 || y > h - 1)
    {
        for (int i = 0; i < 3; i++)
        {
            // Populate with zeroes
            rgb[i] = 0;
        }
        // Exit
        return;
    }

    // If at exact pixel
    if (floor(x) == x && floor(y) == y)
    {
        for (int i = 0; i < 3; i++)
        {
            // Populate with pixel values
            rgb[i] = image[(int)y*w+(int)x][i];
        }
        // Exit
        return;
    }

    // Specify the nearest integer row/cols
    int R1 = floor(y);
    int R2 = ceil(y);
    int C1 = floor(x);
    int C2 = ceil(x);

    // 1 / ((x2 - x1)(y2 - y1))
    double scalar = 1 / ((C2 - C1)*(R2 - R1));
    // RGB = 3
    for (int i = 0; i < 3; i++)
    {
        rgb[i] = scalar * (
                image[R1*w+C1][i] * ((double)C2 - x) * ((double)R2 - y) +
                image[R1*w+C2][i] * (x - (double)C1) * ((double)R2 - y) +
                image[R2*w+C1][i] * ((double)C2 - x) * (y - (double)R1) +
                image[R2*w+C2][i] * (x - (double)C1) * (y - (double)R1)
            );
    }
}

/*******************************************************************************
 Here is the code provided for rotating an image. 'orien' is in degrees.
********************************************************************************/

// Rotating an image by "orien" degrees
void MainWindow::RotateImage(double** image, double orien)

{
    double radians = -2.0*3.141*orien/360.0;

    // Make a copy of the original image and then re-initialize the original image with 0
    double** buffer = new double* [imageWidth*imageHeight];
    for (int i = 0; i < imageWidth*imageHeight; i++)
    {
        buffer[i] = new double [3];
        for(int j = 0; j < 3; j++)
            buffer[i][j] = image[i][j];
        image[i] = new double [3](); // re-initialize to 0
    }

    for (int r = 0; r < imageHeight; r++)
       for (int c = 0; c < imageWidth; c++)
       {
            // Rotate around the center of the image
            double x0 = (double) (c - imageWidth/2);
            double y0 = (double) (r - imageHeight/2);

            // Rotate using rotation matrix
            double x1 = x0*cos(radians) - y0*sin(radians);
            double y1 = x0*sin(radians) + y0*cos(radians);

            x1 += (double) (imageWidth/2);
            y1 += (double) (imageHeight/2);

            double rgb[3];
            BilinearInterpolation(buffer, x1, y1, rgb);

            // Note: "image[r*imageWidth+c] = rgb" merely copies the head pointers of the arrays, not the values
            image[r*imageWidth+c][0] = rgb[0];
            image[r*imageWidth+c][1] = rgb[1];
            image[r*imageWidth+c][2] = rgb[2];
        }
}

/**************************************************
 TASK 8
**************************************************/

// Find the peaks of the edge responses perpendicular to the edges
void MainWindow::FindPeaksImage(double** image, double thres)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * NOTE: image is grayscale here, i.e., all 3 channels have the same value which is the grayscale value
 * thres: threshold value for magnitude
*/
{
    int w = imageWidth;
    int h = imageHeight;

    // Create two empty images to hold the magnitude and orientation
    // Oriens only one dimention because it doesn't need to be passed to bilinear
    double*  oriens = new double  [w*h];
    double** mags   = new double* [w*h];
    for (int i = 0; i < w*h; i++)
    {
        mags[i] = new double[3];
    }

    // Create a copy of the input image to apply the Sobel operator to
    double** sobel = CopyImage(image, w, h);
    // And apply that operator
    SobelImage(sobel);

    // Now, do an inversion of the functions applied to find the mag and orien
    // For each pixel
    for (int i = 0; i < w*h; i++)
    {
        // Find the magnitude
        double mag   = (sobel[i][0] + sobel[i][1] + sobel[i][2]) / 4;
        // Find the orientation
        double orien = asin(sobel[i][0] * 2.0 / (mag * 4.0) - 1.0);
        // Apply the magnitude three times so it can be used as the input for bilinear
        for (int j = 0; j < 3; j++)
        {
            mags[i][j] = mag;
        }
        // Orien only needs one copy because it doesn't need to be passed to a function
        oriens[i] = orien;
    }

    // Find e1, e2, and compare against current pixel and threshold.
    // If it's greater in all cases, set output image to 255; else zero
    // Hold result values
    double rgb[3];
    // For each pixel
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            // Find e (mag)
            double e = mags[r*w+c][0];
            if (e <= thres)  // Less than threshold, skip further computation
                for (int i = 0; i < 3; i++)
                {
                    image[r*w+c][i] = 0;
                }
                continue;

            // Find theta
            double theta = oriens[r*w+c];
            // Find e1_position
            double e1x   = c + cos(theta);
            double e1y   = r + sin(theta);
            // Find e2_position
            double e2x   = c - cos(theta);
            double e2y   = r - sin(theta);

            // Find rgb at e1
            BilinearInterpolation(mags, e1x, e1y, rgb);
            // Find e1
            double e1 = rgb[0];  // All values should be equal
            // Fidn rgb at e2
            BilinearInterpolation(mags, e2x, e2y, rgb);
            // Find e2
            double e2 = rgb[0];  // All values should be equal

            // Test for peakiness
            int result;
            if (e > e1 && e > e2)
            {
                result = 255;
            }
            else
            {
                result = 0;
            }
            // And assign
            for (int i = 0; i < 3; i++)
            {
                image[r*w+c][i] = result;
            }
        }

    // Clean up
    DeleteCopy(sobel, w*h);
    DeleteCopy(mags, w*h);
    delete[] oriens;
}

/**************************************************
 TASK 9 (a)
**************************************************/

// Helper function to find the L1 distance between two pixels
double L1(double* a, double* b)
/*
 * a: The first pixel
 * b: The second pixel
*/
{
    return abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2]);
}

// Helper function to find the closest cluster to the current pixel values
int ClosestCluster(double* pixel, double** clusterMeans, int num_clusters)
/*
 * pixel: The pixel to compare values against
 * clusterMeans: The average values of the clusters
*/
{
    // Set initial values
    double closestClusterDistance = L1(pixel, clusterMeans[0]);
    int closestClusterPos = 0;

    // Iterate through looking for better values
    for (int cluster = 1; cluster < num_clusters; cluster++)
    {
        double currentClusterDistance = L1(pixel, clusterMeans[cluster]);
        if (currentClusterDistance < closestClusterDistance)
        {
            closestClusterDistance = currentClusterDistance;
            closestClusterPos      = cluster;
        }
    }

    return closestClusterPos;
}

// Helper function to perform k-means clutering on a color image with parameterized seeds
void Cluster(double** image, double** clusterMeans, double** clusterMeansNext,
             int num_clusters, int w, int h, int epsilon)
{
    double  clusterSumNext  [num_clusters][3];  // The running sum of pixel values in a cluster
    int     clusterCountNext[num_clusters];  // The total pixels in a cluster
    double  totalMeansDistance;  // The distance between the previous and current means
    int  iter = 0;  // Exit after 100

    do
    {
        // Set the next cluster means to the current
        for (int i = 0; i < num_clusters; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                clusterMeans[i][j] = clusterMeansNext[i][j];
            }
        }

        // Reset the cluster counters
        for (int i = 0; i < num_clusters; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                clusterSumNext[i][j] = 0.0;
            }
            clusterCountNext[i] =  0;
        }

        // Add the closest pixels to their respective clusters
        for (int pixel = 0; pixel < w*h; pixel++)
        {
            int closestClusterPos = ClosestCluster(image[pixel], clusterMeans, num_clusters);
            for (int channel = 0; channel < 3; channel++)
            {
                clusterSumNext[closestClusterPos][channel] += image[pixel][channel];
            }
            clusterCountNext[closestClusterPos]++;
        }

        // Find the new means
        for (int i = 0; i < num_clusters; i++)
        {
            // If zero, ignore and maintain values for next iteration
            if (clusterCountNext[i] != 0)
            {
                for (int j = 0; j < 3; j++)
                {
                    // Else, find the new means and assign
                    clusterMeansNext[i][j] = clusterSumNext[i][j] / double(clusterCountNext[i]);
                }
            }
        }

        // Add to the iteration variable
        iter++;

        // Update the sum of the L1 distances
        totalMeansDistance = 0;
        for (int i = 0; i < num_clusters; i++)
        {
            totalMeansDistance += L1(clusterMeansNext[i], clusterMeans[i]);
        }

    } while (iter < 100 && totalMeansDistance < epsilon * num_clusters);


    // Finally, assign the colors to the image
    for (int i = 0; i < w*h; i++)
    {
        image[i] = clusterMeansNext[ClosestCluster(image[i], clusterMeansNext, num_clusters)];
    }
}

// Perform K-means clustering on a color image using random seeds
void MainWindow::RandomSeedImage(double** image, int num_clusters)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * num_clusters: number of clusters into which the image is to be clustered
*/
{
    int w = imageWidth;
    int h = imageHeight;

    // Defines how soon the algorithm completes
    int epsilon = 30;
    // Holds the current cluster averages
    // (must be built on the heap to be passed)
    double** clusterMeans = new double* [num_clusters];
    for (int i = 0; i < num_clusters; i++)
    {
        clusterMeans[i] = new double[3];
    }
    // Holds the cluster averages for the next iteration
    double** clusterMeansNext = new double* [num_clusters];
    for (int i = 0; i < num_clusters; i++)
    {
        clusterMeansNext[i] = new double[3];
        // Use random seeds for the clusters
        for (int j = 0; j < 3; j++)
        {
            clusterMeansNext[i][j] = rand() % 256;
        }
    }

    Cluster(image, clusterMeans, clusterMeansNext, num_clusters, w, h, epsilon);
}

/**************************************************
 TASK 9 (b)
**************************************************/

// Perform K-means clustering on a color image using seeds from the image itself
void MainWindow::PixelSeedImage(double** image, int num_clusters)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * num_clusters: number of clusters into which the image is to be clustered
*/
{
    int w = imageWidth;
    int h = imageHeight;

    // Defines how soon the algorithm completes
    int epsilon = 30;
    // Holds the current cluster averages
    // (must be built on the heap to be passed)
    double** clusterMeans = new double* [num_clusters];
    for (int i = 0; i < num_clusters; i++)
    {
        clusterMeans[i] = new double[3];
    }
    // Holds the cluster averages for the next iteration
    double** clusterMeansNext = new double* [num_clusters];
    // For every cluster we need to initialize
    for (int cluster = 0; cluster < num_clusters; cluster++)
    {
        clusterMeansNext[cluster] = new double[3];
        // Choose pixels from the image that are L1 >= 100 from other selected pixels
        // Look at each pixel
        for (int pixel = 0; pixel < w*h; pixel++)
        {
            // And compare to the already chosen clusters
            for (int clusterCompare = 0; clusterCompare < cluster; clusterCompare++)
            {
                // If L1 is too small for any of the clusters
                if (L1(image[pixel], clusterMeansNext[clusterCompare]) < 100)
                    // Try the next pixel
                    goto nextPixel;
            }
            // If the pixel is a good choice
            for (int channel = 0; channel < 3; channel++)
            {
                // Assign the pixel as a cluster
                clusterMeansNext[cluster][channel] = image[pixel][channel];
            }
            // And move on to the next cluster
            goto nextCluster;
            nextPixel: ;
        }
        nextCluster: ;
    }

    Cluster(image, clusterMeans, clusterMeansNext, num_clusters, w, h, epsilon);
}


/**************************************************
 EXTRA CREDIT TASKS
**************************************************/

// Perform K-means clustering on a color image using the color histogram
void MainWindow::HistogramSeedImage(double** image, int num_clusters)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * num_clusters: number of clusters into which the image is to be clustered
*/
{
    // Add your code here
}

// Apply the median filter on a noisy image to remove the noise
void MainWindow::MedianImage(double** image, int radius)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * radius: radius of the kernel
*/
{
    // Add your code here
}

// Apply Bilater filter on an image
void MainWindow::BilateralImage(double** image, double sigmaS, double sigmaI)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
 * sigmaS: standard deviation in the spatial domain
 * sigmaI: standard deviation in the intensity/range domain
*/
{
    // Add your code here.  Should be similar to GaussianBlurImage.
}

// Perform the Hough transform
void MainWindow::HoughImage(double** image)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
*/
{
    // Add your code here
}

// Perform smart K-means clustering
void MainWindow::SmartKMeans(double** image)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight)*3 having double values
*/
{
    // Add your code here
}
