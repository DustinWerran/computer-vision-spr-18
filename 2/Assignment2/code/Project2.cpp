#include "mainwindow.h"
#include "math.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include "Matrix.h"

/*******************************************************************************
    The following are helper routines with code already written.
    The routines you'll need to write for the assignment are below.
*******************************************************************************/

/*******************************************************************************
Draw detected Harris corners
    cornerPts - corner points
    numCornerPts - number of corner points
    imageDisplay - image used for drawing

    Draws a red cross on top of detected corners
*******************************************************************************/
void MainWindow::DrawCornerPoints(CIntPt *cornerPts, int numCornerPts, QImage &imageDisplay)
{
   int i;
   int r, c, rd, cd;
   int w = imageDisplay.width();
   int h = imageDisplay.height();

   for(i=0;i<numCornerPts;i++)
   {
       c = (int) cornerPts[i].m_X;
       r = (int) cornerPts[i].m_Y;

       for(rd=-2;rd<=2;rd++)
           if(r+rd >= 0 && r+rd < h && c >= 0 && c < w)
               imageDisplay.setPixel(c, r + rd, qRgb(255, 0, 0));

       for(cd=-2;cd<=2;cd++)
           if(r >= 0 && r < h && c + cd >= 0 && c + cd < w)
               imageDisplay.setPixel(c + cd, r, qRgb(255, 0, 0));
   }
}

/*******************************************************************************
Compute corner point descriptors
    image - input image
    cornerPts - array of corner points
    numCornerPts - number of corner points

    If the descriptor cannot be computed, i.e. it's too close to the boundary of
    the image, its descriptor length will be set to 0.

    I've implemented a very simple 8 dimensional descriptor.  Feel free to
    improve upon this.
*******************************************************************************/
void MainWindow::ComputeDescriptors(QImage image, CIntPt *cornerPts, int numCornerPts)
{
    int r, c, cd, rd, i, j;
    int w = image.width();
    int h = image.height();
    double *buffer = new double [w*h];
    QRgb pixel;

    // Descriptor parameters
    double sigma = 2.0;
    int rad = 4;

    // Computer descriptors from green channel
    for(r=0;r<h;r++)
       for(c=0;c<w;c++)
        {
            pixel = image.pixel(c, r);
            buffer[r*w + c] = (double) qGreen(pixel);
        }

    // Blur
    GaussianBlurImage(buffer, w, h, sigma);

    // Compute the desciptor from the difference between the point sampled at its center
    // and eight points sampled around it.
    for(i=0;i<numCornerPts;i++)
    {
        int c = (int) cornerPts[i].m_X;
        int r = (int) cornerPts[i].m_Y;

        if(c >= rad && c < w - rad && r >= rad && r < h - rad)
        {
            double centerValue = buffer[(r)*w + c];
            int j = 0;

            for(rd=-1;rd<=1;rd++)
                for(cd=-1;cd<=1;cd++)
                    if(rd != 0 || cd != 0)
                {
                    cornerPts[i].m_Desc[j] = buffer[(r + rd*rad)*w + c + cd*rad] - centerValue;
                    j++;
                }

            cornerPts[i].m_DescSize = DESC_SIZE;
        }
        else
        {
            cornerPts[i].m_DescSize = 0;
        }
    }

    delete [] buffer;
}

/*******************************************************************************
Draw matches between images
    matches - matching points
    numMatches - number of matching points
    image1Display - image to draw matches
    image2Display - image to draw matches

    Draws a green line between matches
*******************************************************************************/
void MainWindow::DrawMatches(CMatches *matches, int numMatches, QImage &image1Display, QImage &image2Display)
{
    int i;
    // Show matches on image
    QPainter painter;
    painter.begin(&image1Display);
    QColor green(0, 250, 0);
    QColor red(250, 0, 0);

    for(i=0;i<numMatches;i++)
    {
        painter.setPen(green);
        painter.drawLine((int) matches[i].m_X1, (int) matches[i].m_Y1, (int) matches[i].m_X2, (int) matches[i].m_Y2);
        painter.setPen(red);
        painter.drawEllipse((int) matches[i].m_X1-1, (int) matches[i].m_Y1-1, 3, 3);
    }

    QPainter painter2;
    painter2.begin(&image2Display);
    painter2.setPen(green);

    for(i=0;i<numMatches;i++)
    {
        painter2.setPen(green);
        painter2.drawLine((int) matches[i].m_X1, (int) matches[i].m_Y1, (int) matches[i].m_X2, (int) matches[i].m_Y2);
        painter2.setPen(red);
        painter2.drawEllipse((int) matches[i].m_X2-1, (int) matches[i].m_Y2-1, 3, 3);
    }

}


/*******************************************************************************
Given a set of matches computes the "best fitting" homography
    matches - matching points
    numMatches - number of matching points
    h - returned homography
    isForward - direction of the projection (true = image1 -> image2, false = image2 -> image1)
*******************************************************************************/
bool MainWindow::ComputeHomography(CMatches *matches, int numMatches, double h[3][3], bool isForward)
{
    int error;
    int nEq=numMatches*2;

    dmat M=newdmat(0,nEq,0,7,&error);
    dmat a=newdmat(0,7,0,0,&error);
    dmat b=newdmat(0,nEq,0,0,&error);

    double x0, y0, x1, y1;

    for (int i=0;i<nEq/2;i++)
    {
        if(isForward == false)
        {
            x0 = matches[i].m_X1;
            y0 = matches[i].m_Y1;
            x1 = matches[i].m_X2;
            y1 = matches[i].m_Y2;
        }
        else
        {
            x0 = matches[i].m_X2;
            y0 = matches[i].m_Y2;
            x1 = matches[i].m_X1;
            y1 = matches[i].m_Y1;
        }


        //Eq 1 for corrpoint
        M.el[i*2][0]=x1;
        M.el[i*2][1]=y1;
        M.el[i*2][2]=1;
        M.el[i*2][3]=0;
        M.el[i*2][4]=0;
        M.el[i*2][5]=0;
        M.el[i*2][6]=(x1*x0*-1);
        M.el[i*2][7]=(y1*x0*-1);

        b.el[i*2][0]=x0;
        //Eq 2 for corrpoint
        M.el[i*2+1][0]=0;
        M.el[i*2+1][1]=0;
        M.el[i*2+1][2]=0;
        M.el[i*2+1][3]=x1;
        M.el[i*2+1][4]=y1;
        M.el[i*2+1][5]=1;
        M.el[i*2+1][6]=(x1*y0*-1);
        M.el[i*2+1][7]=(y1*y0*-1);

        b.el[i*2+1][0]=y0;

    }
    int ret=solve_system (M,a,b);
    if (ret!=0)
    {
        freemat(M);
        freemat(a);
        freemat(b);

        return false;
    }
    else
    {
        h[0][0]= a.el[0][0];
        h[0][1]= a.el[1][0];
        h[0][2]= a.el[2][0];

        h[1][0]= a.el[3][0];
        h[1][1]= a.el[4][0];
        h[1][2]= a.el[5][0];

        h[2][0]= a.el[6][0];
        h[2][1]= a.el[7][0];
        h[2][2]= 1;
    }

    freemat(M);
    freemat(a);
    freemat(b);

    return true;
}


/*******************************************************************************
*******************************************************************************
*******************************************************************************

    The routines you need to implement are below

*******************************************************************************
*******************************************************************************
*******************************************************************************/

// Normalize the values of the kernel to sum-to-one
void NormalizeKernel(double *kernel, int kernelWidth, int kernelHeight)
{
    double denom = 0.000001; int i;
    for(i=0; i<kernelWidth*kernelHeight; i++)
        denom += kernel[i];
    for(i=0; i<kernelWidth*kernelHeight; i++)
        kernel[i] /= denom;
}

// Convolve the image with the kernel
void Convolution(double* image, double *kernel, int w, int h, int kernelWidth, int kernelHeight, bool add)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight) having double values
 * kernel: 1-D array of kernel values
 * kernelWidth: width of the kernel
 * kernelHeight: height of the kernel
 * add: a boolean variable (taking values true or false)
*/
{
    // Width and height must be odd
    if (kernelWidth % 2 == 0 || kernelHeight % 2 == 0)
    {
        return;
    }

    // Create a buffer image so we're not reading and writing to the same image during filtering.
    // This creates an image of size (w + kernelWidth - 1, h + kernelHeight - 1) with black borders (zero-padding)
    int pad = (w + kernelWidth - 1) * (h + kernelHeight - 1);
    double* buffer = new double[pad];
    for (int i = 0; i < pad; i++)
    {
        buffer[i] = 0;  // create black borders
    }

    // For each pixel in the original
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            int bufferPos = 0;                                      // This assignment is for clarity; it will optimize away
            bufferPos += kernelHeight / 2 * (w + kernelWidth - 1);  // Skip the top edge
            bufferPos += (w + kernelWidth - 1) * r;                 // Forward to the current row
            bufferPos += kernelWidth / 2;                           // Skip the left edge in the current row
            bufferPos += c;                                         // Forward to the current column

            // Assign the channel value to the double form of the image
            buffer[bufferPos] = image[r*w+c];
        }

    // For each pixel in the image...
    for (int r = 0; r < h; r++)
    {
        for (int c = 0; c < w; c++)
        {
            double value = 0;

            // Convolve the kernel at each pixel
            for (int rd = 0; rd < kernelHeight; rd++)
                for (int cd = 0; cd < kernelWidth; cd++)
                {
                     // Get the pixel value
                     double pixel = buffer[(r + rd) * (w + kernelWidth - 1) + c + cd];

                     // Get the value of the kernel
                     double weight = kernel[rd * kernelWidth + cd];

                     value += weight*pixel;
                }
            // Store the pixel in the image to be returned
            if (add)
            {
                value += 128;
            }
            image[r*w+c] = value;
        }
    }
    // Clean up
    delete[] buffer;
}

/*******************************************************************************
Blur a single channel floating point image with a Gaussian.
    image - input and output image
    w - image width
    h - image height
    sigma - standard deviation of Gaussian

    This code should be very similar to the code you wrote for assignment 1.
*******************************************************************************/
void MainWindow::GaussianBlurImage(double *image, int w, int h, double sigma)
{
    // The radius of the Gaussian blur
    int radius = (int)(ceil(3*sigma));
    // The kernel is square
    int kernelWidth = 2*radius+1, kernelHeight = 2*radius+1;

    // Initialize the kernel
    double* kernel = new double[kernelWidth * kernelHeight];

    // Calculate the scalar for this sigma (used in the Gaussian function)
    double scalar = 1.0 / (2 * M_PI * sigma * sigma);

    // For each row
    for (int hr = -radius; hr <= radius; hr++)
    {
        // For each column
        for (int wr = -radius; wr <= radius; wr++)
        {
            // Calculate and assign the kernel value for the Gaussian blur at that location
            kernel[(hr + radius) * kernelWidth + (wr + radius)]
                = scalar * exp(- (hr * hr + wr * wr) / (2 * sigma * sigma));
        }
    }

    // Normalize the kernel to sum to 1
    NormalizeKernel(kernel, kernelWidth, kernelHeight);

    // Convolve the kernel with the image, don't set add
    Convolution(image, kernel, w, h, kernelWidth, kernelHeight, false);

    delete[] kernel;
}

// Compute the First derivative of an image along the horizontal direction.
void FirstDerivImage_x(double* image, int w, int h)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight) having double values
 * sigma: standard deviation for the Gaussian kernel
*/
{
    // Set the kernel dimentions
    int kernelWidth  = 3;
    int kernelHeight = 1;

    // Initialize the kernel
    double* kernel = new double[3];
    kernel[0] = -1, kernel[1] = 0, kernel[2] = 1;

    // Convolve the kernel with the image, don't set add
    Convolution(image, kernel, w, h, kernelWidth, kernelHeight, false);

    delete[] kernel;
}

// Compute the First derivative of an image along the vertical direction.
void FirstDerivImage_y(double* image, int w, int h)
/*
 * image: input image in matrix form of size (imageWidth*imageHeight) having double values
 * sigma: standard deviation for the Gaussian kernel
*/
{
    // Set the kernel dimentions
    int kernelWidth  = 1;
    int kernelHeight = 3;

    // Initialize the kernel
    double* kernel = new double[3];
    kernel[0] = -1, kernel[1] = 0, kernel[2] = 1;

    // Convolve the kernel with the image, don't set add
    Convolution(image, kernel, w, h, kernelWidth, kernelHeight, false);

    delete[] kernel;
}

// Helper function to find corner response
double CornerResponse(int r, int c, double* I_x2, double* I_y2, double* I_xy, int w)
{
    int radius = 2;

    double sums[4] = {0,0,0,0};
    for (int rr = -radius; rr <= radius; rr++)
    {
       for (int cr = -radius; cr <= radius; cr++)
       {
          sums[0] += I_x2[(r + rr) * w + (c + cr)];
          sums[1] += I_xy[(r + rr) * w + (c + cr)];
          sums[2] += I_xy[(r + rr) * w + (c + cr)];
          sums[3] += I_y2[(r + rr) * w + (c + cr)];
       }
    }

    return (sums[0] * sums[3] - sums[1] * sums[2]) / (sums[0] + sums[3]);
}

// Helper function to perform nonmax suppresion on an input image.
int NonmaximumSuppresion(double* image, int w, int h, double* maxima)
{
/*
 * image: image to perform nonmax suppresion on
 * w: width of the image
 * h: height of the image
 * maxima: vector populated with maxima where present; zero everywhere else
 * return: number of maxima found
*/
    // fill the image with zeroes
    for (int pixel = 0; pixel < w*h; pixel++)
    {
        maxima[pixel] = 0;
    }

    int count = 0;

    // for each pixel (except the edges)
    for (int r = 4; r < h - 4; r++)
    {
        for (int c = 4; c < w - 4; c++)
        {
            if (
                image[r*w+c] > image[r*w+c+1]     &&
                image[r*w+c] > image[r*w+c-1]     &&
                image[r*w+c] > image[(r-1)*w+c-1] &&
                image[r*w+c] > image[(r+1)*w+c+1] &&
                image[r*w+c] > image[(r+1)*w+c-1] &&
                image[r*w+c] > image[(r-1)*w+c+1] &&
                image[r*w+c] > image[(r+1)*w+c]   &&
                image[r*w+c] > image[(r-1)*w+c]
            )
            {
                maxima[r*w+c] = image[r*w+c];
                count++;
            }
        }
    }

    return count;
}

/*******************************************************************************
Detect Harris corners.
    image - input image
    sigma - standard deviation of Gaussian used to blur corner detector
    thres - Threshold for detecting corners
    cornerPts - returned corner points
    numCornerPts - number of corner points returned
    imageDisplay - image returned to display (for debugging)
*******************************************************************************/
void MainWindow::HarrisCornerDetector(QImage image, double sigma, double thres, CIntPt **cornerPts, int &numCornerPts, QImage &imageDisplay)
{
    int r, c;
    int w = image.width();
    int h = image.height();
    double *buffer = new double [w*h];
    QRgb pixel;

    numCornerPts = 0;

    // Compute the corner response using just the green channel
    for(r=0;r<h;r++)
       for(c=0;c<w;c++)
       {
           pixel = image.pixel(c, r);
           buffer[r*w + c] = (double) qGreen(pixel);
       }

    // Apply the gaussian first to simplify later calculations
    GaussianBlurImage(buffer, w, h, sigma);

    double* I_x2 = new double[w*h];
    double* I_y2 = new double[w*h];
    double* I_xy = new double[w*h];

    // Copy the buffer into the two images to be differentiated
    for (int i = 0; i < w*h; i++)
    {
        I_x2[i] = I_y2[i] = buffer[i];
    }

    FirstDerivImage_x(I_x2, w, h);
    FirstDerivImage_y(I_y2, w, h);

    // For each pixel
    for (int i = 0; i < w*h; i++)
    {
        I_xy[i] = I_x2[i] * I_y2[i];  // compute the product
        I_x2[i] = I_x2[i] * I_x2[i];  // compute the square of x dir
        I_y2[i] = I_y2[i] * I_y2[i];  // compute the square of y dir
    }

    int radius = 2;
    // for all pixels except the edges
    for (int r = radius; r < h - radius; r++)
    {
        for (int c = radius; c < w - radius; c++)
        {
            int response = CornerResponse(r, c, I_x2, I_y2, I_xy, w);
            if (response < thres)
            {
                buffer[r*w+c] = 0;
            }
            else
            {
                buffer[r*w+c] = response;
            }
        }
    }

    double* maxima = new double[w*h];
    numCornerPts = NonmaximumSuppresion(buffer, w, h, maxima);

    *cornerPts = new CIntPt [numCornerPts];
    int i = 0;
    for (int y = 0; y < h; y++)
    {
        for (int x = 0; x < w; x++)
        {
            if (maxima[y*w+x] != 0)
            {
                (*cornerPts)[i].m_X = x;
                (*cornerPts)[i].m_Y = y;
                i++;
            }
        }
    }

    // Once you are done finding the corner points, display them on the image
    DrawCornerPoints(*cornerPts, numCornerPts, imageDisplay);

    delete [] buffer;
    delete [] maxima;
}

// Helper function to find L1 Distance between two points (normalized)
double L1(CIntPt pt1, CIntPt pt2)
{
    double ret = 0;

    // if descriptors are not zero
    if (pt1.m_DescSize && pt2.m_DescSize)
        // assumes same-size descriptor
        for (int i = 0; i < pt1.m_DescSize; i++)
        {
            ret += abs(pt1.m_Desc[i] - pt2.m_Desc[i]);
        }

    return ret;
}

/*******************************************************************************
Find matching corner points between images.
    image1 - first input image
    cornerPts1 - corner points corresponding to image 1
    numCornerPts1 - number of corner points in image 1
    image2 - second input image
    cornerPts2 - corner points corresponding to image 2
    numCornerPts2 - number of corner points in image 2
    matches - set of matching points to be returned
    numMatches - number of matching points returned
    image1Display - image used to display matches
    image2Display - image used to display matches
*******************************************************************************/
void MainWindow::MatchCornerPoints(QImage image1, CIntPt *cornerPts1, int numCornerPts1,
                             QImage image2, CIntPt *cornerPts2, int numCornerPts2,
                             CMatches **matches, int &numMatches, QImage &image1Display, QImage &image2Display)
{
    numMatches = 0;

    // Compute the descriptors for each corner point.
    // You can access the descriptor for each corner point using cornerPts1[i].m_Desc[j].
    // If cornerPts1[i].m_DescSize = 0, it was not able to compute a descriptor for that point
    ComputeDescriptors(image1, cornerPts1, numCornerPts1);
    ComputeDescriptors(image2, cornerPts2, numCornerPts2);

    // We're finding a match for every point in image 1,
    // therefore numMatches will always be equal to numCornerPts1
    numMatches = numCornerPts1;
    *matches = new CMatches [numMatches];

    for (int i = 0; i < numCornerPts1; i++)
    {
        // Set default values
        int closest = 0;
        double min = L1(cornerPts1[i], cornerPts2[0]);

        for (int j = 0; j < numCornerPts2; j++)
        {
            // Find the distance between the current points
            double dist = L1(cornerPts1[i], cornerPts2[j]);
            // If the distance is smaller
            if (dist < min)
            {
                // Save that point
                closest = j;
                min = dist;
            }
        }

        // Assign that pair as the newest match
        (*matches)[i].m_X1 = cornerPts1[i].m_X;
        (*matches)[i].m_Y1 = cornerPts1[i].m_Y;
        (*matches)[i].m_X2 = cornerPts2[closest].m_X;
        (*matches)[i].m_Y2 = cornerPts2[closest].m_Y;
    }

    // Draw the matches
    DrawMatches(*matches, numMatches, image1Display, image2Display);
}

/*******************************************************************************
Project a point (x1, y1) using the homography transformation h
    (x1, y1) - input point
    (x2, y2) - returned point
    h - input homography used to project point
*******************************************************************************/
void MainWindow::Project(double x1, double y1, double &x2, double &y2, double h[3][3])
{
    // Compute u, v, w
    double u = h[0][0]*x1 + h[0][1]*y1 + h[0][2];
    double v = h[1][0]*x1 + h[1][1]*y1 + h[1][2];
    double w = h[2][0]*x1 + h[2][1]*y1 + 1;

    // Compute x', y'
    x2 = u / w;
    y2 = v / w;
}

/*******************************************************************************
Count the number of inliers given a homography.  This is a helper function for RANSAC.
    h - input homography used to project points (image1 -> image2
    matches - array of matching points
    numMatches - number of matchs in the array
    inlierThreshold - maximum distance between points that are considered to be inliers

    Returns the total number of inliers.
*******************************************************************************/
int MainWindow::ComputeInlierCount(double h[3][3], CMatches *matches, int numMatches, double inlierThreshold)
{
    int ret = 0;
    double x1, y1, x2, y2;

    for (int match = 0; match < numMatches; match++)
    {
        x1 = matches[match].m_X1;
        y1 = matches[match].m_Y1;
        Project(x1, y1, x2, y2, h);
        double distance = abs(x2-matches[match].m_X2) + abs(y2-matches[match].m_Y2);
        if (distance < inlierThreshold)
            ret++;
    }

    return ret;
}


/*******************************************************************************
Compute homography transformation between images using RANSAC.
    matches - set of matching points between images
    numMatches - number of matching points
    numIterations - number of iterations to run RANSAC
    inlierThreshold - maximum distance between points that are considered to be inliers
    hom - returned homography transformation (image1 -> image2)
    homInv - returned inverse homography transformation (image2 -> image1)
    image1Display - image used to display matches
    image2Display - image used to display matches
*******************************************************************************/
void MainWindow::RANSAC(CMatches *matches, int numMatches, int numIterations, double inlierThreshold,
                        double hom[3][3], double homInv[3][3], QImage &image1Display, QImage &image2Display)
{
    // Set up variables to hold randomly chosen matches
    CMatches* matchesHomography = new CMatches[4];
    int numMatchesHomography = 4;

    // Set up variables to hold current (working) homography data
    double currentHomography[3][3];
    int currentHomographyInliers;

    // Set up variables to hold best homography data
    double bestHomography[3][3];
    int bestHomographyInliers = 0;

    for (; numIterations > 0; numIterations--)
    {
        // Assign random matches to the test homography
        for (int i = 0; i < 4; i++)
        {
            matchesHomography[i] = matches[rand()%numMatches];
        }

        // Compute the homography for this group of matches
        ComputeHomography(matchesHomography, numMatchesHomography, currentHomography, true);

        // Compute the number of inliers in the total set of matches for this current homography
        currentHomographyInliers = ComputeInlierCount(currentHomography, matches, numMatches, inlierThreshold);

        // If there are more inliers for this homography than ever before
        if (currentHomographyInliers > bestHomographyInliers)
        {
            // Set this as the new best homography
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    bestHomography[i][j] = currentHomography[i][j];
            bestHomographyInliers = currentHomographyInliers;
        }
    }

    int numInliers = bestHomographyInliers;  // For clarity
    // Create an array to hold the inliers for the best homography
    CMatches* inliers = new CMatches[numInliers];

    // And compute them
    double x1, y1, x2, y2;
    int inlierPos = 0;

    for (int match = 0; match < numMatches; match++)
    {
        x1 = matches[match].m_X1;
        y1 = matches[match].m_Y1;
        Project(x1, y1, x2, y2, bestHomography);
        double distance = abs(x2-matches[match].m_X2) + abs(y2-matches[match].m_Y2);
        if (distance < inlierThreshold)
        {
            inliers[inlierPos] = matches[match];
            inlierPos++;
        }
    }

    // Now, compute a refined homograpy for this set of inliers
    ComputeHomography(inliers, numInliers, hom, true);
    // And an inverse homography as well
    ComputeHomography(inliers, numInliers, homInv, false);

    // After you're done computing the inliers, display the corresponding matches.
    DrawMatches(inliers, numInliers, image1Display, image2Display);

}

/*******************************************************************************
Bilinearly interpolate image (helper function for Stitch)
    image - input image
    (x, y) - location to interpolate
    rgb - returned color values

    You can just copy code from previous assignment.
*******************************************************************************/
bool MainWindow::BilinearInterpolation(QImage *image, double x, double y, double rgb[3])
{
    int w = image->width();
    int h = image->height();

    // If outside image
    if (x < 0 || x > w - 1 || y < 0 || y > h - 1)
    {
        return false;
    }

    // If at exact pixel
    if (floor(x) == x && floor(y) == y)
    {
        QRgb pixel = image->pixel((int)x, (int)y);
        rgb[0] = (double) qRed(pixel);
        rgb[1] = (double) qGreen(pixel);
        rgb[2] = (double) qBlue(pixel);
        // Exit
        return true;
    }

    // Specify the nearest integer row/cols
    int R1 = floor(y);
    int R2 = ceil(y);
    int C1 = floor(x);
    int C2 = ceil(x);

    // 1 / ((x2 - x1)(y2 - y1))
    double scalar = 1 / ((C2 - C1)*(R2 - R1));
    // RGB = 3

    QRgb pixel1 = image->pixel(C1, R1);
    QRgb pixel2 = image->pixel(C2, R1);
    QRgb pixel3 = image->pixel(C1, R2);
    QRgb pixel4 = image->pixel(C2, R2);

    rgb[0] = scalar * (
            qRed(pixel1) * ((double)C2 - x) * ((double)R2 - y) +
            qRed(pixel2) * (x - (double)C1) * ((double)R2 - y) +
            qRed(pixel3) * ((double)C2 - x) * (y - (double)R1) +
            qRed(pixel4) * (x - (double)C1) * (y - (double)R1)
        );

    rgb[1] = scalar * (
            qGreen(pixel1) * ((double)C2 - x) * ((double)R2 - y) +
            qGreen(pixel2) * (x - (double)C1) * ((double)R2 - y) +
            qGreen(pixel3) * ((double)C2 - x) * (y - (double)R1) +
            qGreen(pixel4) * (x - (double)C1) * (y - (double)R1)
        );

    rgb[2] = scalar * (
            qBlue(pixel1) * ((double)C2 - x) * ((double)R2 - y) +
            qBlue(pixel2) * (x - (double)C1) * ((double)R2 - y) +
            qBlue(pixel3) * ((double)C2 - x) * (y - (double)R1) +
            qBlue(pixel4) * (x - (double)C1) * (y - (double)R1)
        );

    return true;
}


/*******************************************************************************
Stitch together two images using the homography transformation
    image1 - first input image
    image2 - second input image
    hom - homography transformation (image1 -> image2)
    homInv - inverse homography transformation (image2 -> image1)
    stitchedImage - returned stitched image
*******************************************************************************/
void MainWindow::Stitch(QImage image1, QImage image2, double hom[3][3], double homInv[3][3], QImage &stitchedImage)
{
    // Width and height of stitchedImage
    int ws = image1.width();
    int hs = image1.height();

    // Origin of image1 in the stitched image domain
    // (i.e. where image1 will be placed in the stitched image)
    int newOriginX = 0;
    int newOriginY = 0;

    // Save corners of image2
    CIntPt* corners = new CIntPt[4];
    corners[0].m_X = 0; corners[0].m_Y = 0;
    corners[1].m_X = image2.width(); corners[1].m_Y = 0;
    corners[2].m_X = 0; corners[2].m_Y = image2.height();
    corners[3].m_X = image2.width(); corners[3].m_Y =image2.height();

    double projectX, projectY;
    for (int i = 0; i < 4; i++)
    {
        // Project the corner into the domain of image 1
        Project(corners[i].m_X, corners[i].m_Y, projectX, projectY, homInv);

        // X direction
        // If the projected x is farther to the left
        if (projectX < -newOriginX)
        {
            // Set the left edge to the projected X
            ws += ceil(-projectX) - newOriginX;
            // Adjust the origin
            newOriginX = ceil(-projectX);
        }
        // If the projected X is farther to the right
        if (projectX > ws - newOriginX)
        {
            // Set the right edge to the projected X
            ws = ceil(projectX) + newOriginX;
        }

        // Y direction
        // If the projected Y is farther up
        if (projectY < -newOriginY)
        {
            // Set the upper edge to the projected Y
            hs += ceil(-projectY) - newOriginY;
            // Adjust the origin
            newOriginY = ceil(-projectY);
        }
        // if the projected Y is farther down
        if (projectY > hs - newOriginY)
        {
            // Set the lower edge to the projected Y
            hs = ceil(projectY) + newOriginY;
        }
    }

    // Won't need this
    delete[] corners;

    stitchedImage = QImage(ws, hs, QImage::Format_RGB32);
    stitchedImage.fill(qRgb(0,0,0));

    // Since the stitchedImage and image1 space are the same, we can just directly insert it
    for (int y = 0; y < image1.height(); y++)
        for (int x = 0; x < image1.width(); x++)
        {
            // Don't forget to adjust for the new origin
            stitchedImage.setPixel(x + newOriginX, y + newOriginY, image1.pixel(x, y));
        }

    double rgb[3];
    // For each pixel in the stitched image
    for (int y = 0; y < stitchedImage.height(); y++)
        for (int x = 0; x < stitchedImage.width(); x++)
        {
            // Project the pixel into image2. Make sure to adjust for difference in origin
            Project((double)x - newOriginX, (double)y - newOriginY, projectX, projectY, hom);
            // If the coordinate is within image2, this will return the value of it
            if (BilinearInterpolation(&image2, projectX, projectY, rgb))
            {
                // Assign it to the stitched image
                QRgb pixel = qRgb((int)rgb[0], (int)rgb[1], (int)rgb[2]);
                stitchedImage.setPixel(x, y, pixel);
            }
        }
}

