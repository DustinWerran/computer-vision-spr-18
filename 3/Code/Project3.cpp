#include "mainwindow.h"
#include "math.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include "stdlib.h"
#include <algorithm>


/**************************************************
CODE FOR K-MEANS COLOR IMAGE CLUSTERING (RANDOM SEED)
**************************************************/

void Clustering(QImage *image, int num_clusters, int maxit)
{
        int w = image->width(), h = image->height();
        QImage buffer = image->copy();

        std::vector<QRgb> centers, centers_new;

        //initialize random centers
        int n = 1;
        while (n <= num_clusters)
        {
            QRgb center = qRgb(rand() % 256, rand() % 256, rand() % 256);
            centers.push_back(center);
            centers_new.push_back(center);
            n++;
        }

        //iterative part
        int it = 0;
        std::vector<int> ids;
        while (it < maxit)
        {
            ids.clear();
            //assign pixels to clusters
            for (int r = 0; r < h; r++)
                for (int c = 0; c < w; c++)
                {
                    int maxd = 999999, id = 0;
                    for (int n = 0; n < num_clusters; n++)
                    {
                            QRgb pcenter = centers[n];
                            QRgb pnow = buffer.pixel(c, r);
                            int d = abs(qRed(pcenter) - qRed(pnow)) + abs(qGreen(pcenter) - qGreen(pnow)) + abs(qBlue(pcenter) - qBlue(pnow));
                            if (d < maxd)
                            {
                                    maxd = d; id = n;
                            }
                    }
                    ids.push_back(id);
                }

            //update centers
            std::vector<int> cnt, rs, gs, bs;

            for (int n = 0; n < num_clusters; n++)
            {
                rs.push_back(0); gs.push_back(0); bs.push_back(0); cnt.push_back(0);
            }

            for (int r = 0; r < h; r++)
                for (int c = 0; c < w; c++)
                {
                    QRgb pixel = buffer.pixel(c,r);
                    rs[ids[r * w + c]] += qRed(pixel);
                    gs[ids[r * w + c]] += qGreen(pixel);
                    bs[ids[r * w + c]] += qBlue(pixel);
                    cnt[ids[r * w + c]]++;
                }

            for (int n = 0; n < num_clusters; n++)
                if (cnt[n] == 0) // no pixels in a cluster
                    continue;
                else
                    centers_new[n] = qRgb(rs[n]/cnt[n], gs[n]/cnt[n], bs[n]/cnt[n]);

            centers = centers_new; it++;
        }
        //render results
        for (int r = 0; r < h; r++)
            for (int c = 0; c < w; c++)
                image->setPixel(c, r, qRgb(ids[r * w + c],ids[r * w + c],ids[r * w + c]));
}

/**************************************************
CODE FOR FINDING CONNECTED COMPONENTS
**************************************************/

#include "utils.h"

#define MAX_LABELS 80000

#define I(x,y)   (image[(y)*(width)+(x)])
#define N(x,y)   (nimage[(y)*(width)+(x)])

void uf_union( int x, int y, unsigned int parent[] )
{
    while ( parent[x] )
        x = parent[x];
    while ( parent[y] )
        y = parent[y];
    if ( x != y ) {
        if ( y < x ) parent[x] = y;
        else parent[y] = x;
    }
}

int next_label = 1;

int uf_find( int x, unsigned int parent[], unsigned int label[] )
{
    while ( parent[x] )
        x = parent[x];
    if ( label[x] == 0 )
        label[x] = next_label++;
    return label[x];
}

void conrgn(int *image, int *nimage, int width, int height)
{
    unsigned int parent[MAX_LABELS], labels[MAX_LABELS];
    int next_region = 1, k;

    memset( parent, 0, sizeof(parent) );
    memset( labels, 0, sizeof(labels) );

    for ( int y = 0; y < height; ++y )
    {
        for ( int x = 0; x < width; ++x )
        {
            k = 0;
            if ( x > 0 && I(x-1,y) == I(x,y) )
                k = N(x-1,y);
            if ( y > 0 && I(x,y-1) == I(x,y) && N(x,y-1) < k )
                k = N(x,y-1);
            if ( k == 0 )
            {
                k = next_region; next_region++;
            }
            if ( k >= MAX_LABELS )
            {
                fprintf(stderr, "Maximum number of labels reached. Increase MAX_LABELS and recompile.\n"); exit(1);
            }
            N(x,y) = k;
            if ( x > 0 && I(x-1,y) == I(x,y) && N(x-1,y) != k )
                uf_union( k, N(x-1,y), parent );
            if ( y > 0 && I(x,y-1) == I(x,y) && N(x,y-1) != k )
                uf_union( k, N(x,y-1), parent );
        }
    }
    for ( int i = 0; i < width*height; ++i )
        if ( nimage[i] != 0 )
            nimage[i] = uf_find( nimage[i], parent, labels );

    next_label = 1; // reset its value to its initial value
    return;
}


/**************************************************
 **************************************************
TIME TO WRITE CODE
**************************************************
**************************************************/


/**************************************************
Code to compute the features of a given image (both database images and query image)
**************************************************/

std::vector<double*> MainWindow::ExtractFeatureVector(QImage image)
{
    /********** STEP 1 **********/

    // Display the start of execution of this step in the progress box of the application window
    // You can use these 2 lines to display anything you want at any point of time while debugging

    ui->progressBox->append(QString::fromStdString("Clustering.."));
    QApplication::processEvents();

    // Perform K-means color clustering
    // This time the algorithm returns the cluster id for each pixel, not the rgb values of the corresponding cluster center
    // The code for random seed clustering is provided. You are free to use any clustering algorithm of your choice from HW 1
    // Experiment with the num_clusters and max_iterations values to get the best result

    int num_clusters = 5;
    int max_iterations = 50;
    QImage image_copy = image;
    Clustering(&image_copy,num_clusters,max_iterations);


    /********** STEP 2 **********/


    ui->progressBox->append(QString::fromStdString("Connecting components.."));
    QApplication::processEvents();

    // Find connected components in the labeled segmented image
    // Code is given, you don't need to change

    int r, c, w = image_copy.width(), h = image_copy.height();
    int *img = (int*)malloc(w*h * sizeof(int));
    memset( img, 0, w * h * sizeof( int ) );
    int *nimg = (int*)malloc(w*h *sizeof(int));
    memset( nimg, 0, w * h * sizeof( int ) );

    for (r=0; r<h; r++)
        for (c=0; c<w; c++)
            img[r*w + c] = qRed(image_copy.pixel(c,r));

    conrgn(img, nimg, w, h);

    int num_regions=0;
    for (r=0; r<h; r++)
        for (c=0; c<w; c++)
            num_regions = (nimg[r*w+c]>num_regions)? nimg[r*w+c]: num_regions;

    ui->progressBox->append(QString::fromStdString("#regions = "+std::to_string(num_regions)));
    QApplication::processEvents();

    // The resultant image of Step 2 is 'nimg', whose values range from 1 to num_regions

    // Now, thin the smaller regions out
    int threshold = 20;  // The minimum size for a region

    // Create a vector to hold the total number of pixels in each region
    // Initialize to zero
    int* region_counts = new int[num_regions+1]();
    // For each pixel
    for (int pixel = 0; pixel < w*h; pixel++)
        // Increment the counter for that pixel's region
        region_counts[nimg[pixel]]++;

    // Create an array to hold the mapping from original regions to new regions
    // Initialize to zero
    int region_map[num_regions+1] = { 0 };

    // Remove small regions by setting pixels to neighboring large regions
    // NOTE: This algorithm says that the left or above adjacent pixels will always be greater
    // than threshold. This is because they will have already been set to a large region
    // by being processed first. THIS ASSUMPTION DOES NOT HOLD FOR SMALL REGIONS IN THE UPPER
    // LEFT. I've determined it's not worthwhile to revisit those pixels in the rare case
    // they make up a region < threshold.
    for (r = 0; r < h; r++)
        for (c = 0; c < w; c++)
            if (region_counts[nimg[r*w+c]] < threshold)  // If pixel is in small region
            {
                int largest_region = 0;
                int largest_region_size = threshold;
                if (c-1 >= 0)  // If left pixel exists
                {
                    // Will always be greater than threshold, no need to check size
                    largest_region = nimg[r*w+c-1];
                    largest_region_size = region_counts[largest_region];
                }
                if (r-1 >= 0) // If above pixel exists
                {
                    // Will always be greater than threshold, no need to check size
                    largest_region = nimg[(r-1)*w+c];
                    largest_region_size = region_counts[largest_region];
                }
                if (c+1 < w)  // If right pixel exists
                {
                    // Check to see if largest region
                    if (region_counts[nimg[r*w+c+1]] > largest_region_size)
                    {
                        largest_region = nimg[r*w+c+1];
                        largest_region_size = region_counts[largest_region];
                    }
                }
                if (r+1 < h)  // If below pixel exists
                {
                    // Check to see if largest region
                    if (region_counts[nimg[(r+1)*w+c]] > largest_region_size)
                    {
                        largest_region = nimg[(r+1)*w+c];
                        largest_region_size = region_counts[largest_region];
                    }
                }

                // This will only not be true in the upper left corner, if at all
                if (largest_region != 0)              // If we found a region to set to
                {
                    region_counts[nimg[r*w+c]]--;     // Decrement the count for that region
                    if (!region_counts[nimg[r*w+c]])  // If there are no pixels left in that region
                        num_regions--;                // Decrement the number of regions
                    nimg[r*w+c] = largest_region;     // Assign the region the pixel
                }
            }

    int cur = 1;
    // Map the regions (now numbered discontinuously) to a continuous range
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            int region = nimg[r*w+c];
            // If we haven't already set a map for this region
            if (region_map[region] == 0) {
                // Create that map
                region_map[region] = cur;
                // Increment cur so we don't map to the same place
                cur++;
            }
            // Set the region to it's mapping
            nimg[r*w+c] = region_map[region];
        }

    delete[] region_counts;

    /********** STEP 3 **********/


    ui->progressBox->append(QString::fromStdString("Extracting features.."));
    QApplication::processEvents();

    // Extract the feature vector of each region

    // Set length of feature vector according to the number of features you plan to use.
    featurevectorlength = 10;

    // Initializations required to compute feature vector

    std::vector<double*> featurevector; // final feature vector of the image; to be returned
    double **features = new double* [num_regions]; // stores the feature vector for each connected component
    for(int i=0;i<num_regions; i++)
        features[i] = new double[featurevectorlength](); // initialize with zeros

    // Sample code for computing the mean RGB values and size of each connected component

    for(int r=0; r<h; r++)
        for (int c=0; c<w; c++)
        {
            int region = nimg[r*w+c];
            features[region-1][0] += 1;  // stores the number of pixels for each connected component
            features[region-1][1] += (double) qRed(image.pixel(c,r));
            features[region-1][2] += (double) qGreen(image.pixel(c,r));
            features[region-1][3] += (double) qBlue(image.pixel(c,r));
        }

    // Save the mean RGB and size values as image feature after normalization
    for(int m=0; m<num_regions; m++)
    {
        for(int n=1; n<4; n++)
            features[m][n] /= features[m][0]*255.0;
        features[m][0] /= (double) w*h;
        featurevector.push_back(features[m]);
    }

    /********* COMPUTE THE CO-OCCURANCE *********/

    ui->progressBox->append(QString::fromStdString("Creating greyscale.."));
    QApplication::processEvents();

    // Compute the grayscale verison of the image
    QImage grey = image;
    for (int r = 0; r < h; r++)
        for (int c = 0; c < w; c++)
        {
            QRgb pixel = image.pixel(c, r);
            double red = (double) qRed(pixel);
            double green = (double) qGreen(pixel);
            double blue = (double) qBlue(pixel);

            // Compute intensity from colors - these are common weights
            // Quantize the pixels by flooring their values
            double intensity = floor(0.3*red + 0.6*green + 0.1*blue);

            grey.setPixel(c, r, qRgb( (int) intensity, (int) intensity, (int) intensity));
        }

    ui->progressBox->append(QString::fromStdString("Creating matrices.."));
    QApplication::processEvents();

    // Array holding the coocurrance matrix
    double* cooccurrances = new double[256*256];  // 1D cooccurance matrix

    // Int holding the total number of coocurrances for each matrix
    double sum;

    // Do this by region so we don't have to build a huge 3D array to hold many matrices
    for (int region = 1; region <= num_regions; region++)
    {
        // Set the cooccurrance matrix to zero
        for (int i = 0; i < 256*256; i++)
            cooccurrances[i] = 0;
        sum = 0;

        // Skip the edges so we don't run off the image
        for (int r = 0; r < h - 1; r++)
            for (int c = 0; c < w - 1; c++)
            {
                if (nimg[r*w+c] == region && nimg[(r+1)*w+c+1] == region)
                {
                    QRgb pixel = grey.pixel(c, r);
                    QRgb pixel_delta = grey.pixel(c+1, r+1);  // d = (1,1)
                    // Increment the relevant matrix value for the component
                    cooccurrances[256*(int)qRed(pixel)+(int)qRed(pixel_delta)]++;
                    // Increment the sum value for that matrix
                    sum++;
                }
            }

        // Normalize the matrix
        if (sum)
            for (int i = 0; i < 256*256; i++)
            {
                cooccurrances[i] /= sum;
            }

        // Compute the energy
        double energy = 0;
        for (int r = 0; r < 256; r++)
            for (int c = 0; c < 256; c++)
                energy += cooccurrances[r*256+c] * cooccurrances[r*256+c];

        // Compute the entropy
        double entropy = 0;
        for (int r = 0; r < 256; r++)
            for (int c = 0; c < 256; c++)
                // log(0) is nan
                if (cooccurrances[r*256+c])
                    entropy -= cooccurrances[r*256+c] * log(cooccurrances[r*256+c]);

        // Compute the contrast
        double contrast = 0;
        for (int r = 0; r < 256; r++)
            for (int c = 0; c < 256; c++)
                // Normalize the contrast
                contrast += cooccurrances[r*256+c] * (r - c) * (r - c);

        // Assign the values
        features[region-1][4] = energy;
        features[region-1][5] = entropy;
        features[region-1][6] = contrast;
    }

    /********* COMPUTE THE CENTROID *********/

    for (int region = 1; region <= num_regions; region++)
    {
        double r_sum = 0;
        double c_sum = 0;
        double count = 0;

        // Iterate across the pixels
        for (int r = 0; r < h; r++)
            for (int c = 0; c < w; c++)
                // If that pixel is in the region
                if (nimg[r*w+c] == region)
                {
                    // Add its values to the centroid
                    r_sum += r;
                    c_sum += c;
                    count++;
                }

        // Assign the centroid to the feature vector
        features[region-1][7] = r_sum / count;
        features[region-1][8] = c_sum / count;
    }

    /********* COMPUTE THE BOUNDING BOX *********/

    for (int region = 1; region <= num_regions; region++)
    {
        int r_min = h-1;
        int c_min = w-1;
        int r_max = 0;
        int c_max = 0;

        for (int r = 0; r < h; r++)
            for (int c = 0; c < w; c++)
                if (nimg[r*w+c] == region)
                {
                    if (r < r_min) r_min = r;
                    if (c < c_min) c_min = c;
                    if (r > r_max) r_max = r;
                    if (c > c_max) c_max = c;
                }

        // Compute the area
        features[region-1][9] = (r_max - r_min) * (c_max - c_min);
    }

    /********* NORMALIZE *********/

    double max_f4 = 0, max_f5 = 0, max_f6 = 0, max_f7 = 0, max_f8 = 0, max_f9 = 0;
    // Find the maxima
    for (int region = 1; region <= num_regions; region++)
    {
        (features[region-1][4] > max_f4) ? max_f4 = features[region-1][4] : max_f4 = max_f4;
        (features[region-1][5] > max_f5) ? max_f5 = features[region-1][5] : max_f5 = max_f5;
        (features[region-1][6] > max_f6) ? max_f6 = features[region-1][6] : max_f6 = max_f6;
        (features[region-1][7] > max_f7) ? max_f7 = features[region-1][7] : max_f7 = max_f7;
        (features[region-1][8] > max_f8) ? max_f8 = features[region-1][8] : max_f8 = max_f8;
        (features[region-1][9] > max_f9) ? max_f9 = features[region-1][9] : max_f9 = max_f9;
    }
    // Normalize
    for (int region = 1; region <= num_regions; region++)
    {
        features[region-1][4] /= max_f4;
        features[region-1][5] /= max_f5;
        features[region-1][6] /= max_f6;
        features[region-1][7] /= max_f7;
        features[region-1][8] /= max_f8;
        features[region-1][9] /= max_f9;
    }

    // Clean up
    free(img);
    free(nimg);
    delete[] cooccurrances;

    // Return the created feature vector
    ui->progressBox->append(QString::fromStdString("***Done***"));
    QApplication::processEvents();
    return featurevector;
}


/***** Code to compute the distance between two images *****/

// Function that implements distance measure 1
double distance1(double* vector1, double* vector2, int length)
{
    // Euclidian distance
    double ret = 0;
    for (int i = 0; i < length; i++)
    {
        ret += pow((vector1[i] - vector2[i]), 2);
    }
    return sqrt(ret);
}

// Function that implements distance measure 2
double distance2(double* vector1, double* vector2, int length)
{
    // Canberra Distance, k = 2
    double ret = 0;
    for (int i = 0; i < length; i++)
    {
        ret += pow((vector1[i] - vector2[i]), 2) / pow((vector1[1] + vector2[2]), 2);
    }

    return ret;
}

// Function to calculate the distance between two images
// Input argument isOne takes true for distance measure 1 and takes false for distance measure 2

void MainWindow::CalculateDistances(bool isOne)
{
    for(int n=0; n<num_images; n++) // for each image in the database
    {
        distances[n] = 0.0; // initialize to 0 the distance from query image to a database image

        for (int i = 0; i < queryfeature.size(); i++) // for each region in the query image
        {
            double current_distance = (double) RAND_MAX, new_distance;

            for (int j = 0; j < databasefeatures[n].size(); j++) // for each region in the current database image
            {
                if (isOne)
                    new_distance = distance1(queryfeature[i], databasefeatures[n][j], featurevectorlength);
                else
                    new_distance = distance2(queryfeature[i], databasefeatures[n][j], featurevectorlength);

                current_distance = std::min(current_distance, new_distance); // distance between the best matching regions
            }

            distances[n] = distances[n] + current_distance; // sum of distances between each matching pair of regions
        }

        distances[n] = distances[n] / (double) queryfeature.size(); // normalize by number of matching pairs

        // Display the distance values
        ui->progressBox->append(QString::fromStdString("Distance to image "+std::to_string(n+1)+" = "+std::to_string(distances[n])));
        QApplication::processEvents();
    }
}
